from allauth.account.models import EmailAddress
from .models import Users
from django.core.exceptions import PermissionDenied
class VerifiedUserMixin:

    def dispatch(self, request, *args, **kwargs):
        user= self.request.user
        email=EmailAddress.objects.filter(user=user).values().first()
        if email is not None and email["verified"]:
            if Users.objects.filter(email=email["email"]).exists():
                
                return super().dispatch(request, *args, **kwargs)
        raise PermissionDenied
