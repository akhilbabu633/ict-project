from django.contrib import admin
from import_export import resources
from .models import Users
from import_export.admin import ImportExportModelAdmin



# Register your models here.
class UserResource(resources.ModelResource):

    class Meta:
        model = Users
        search_fields = ['user_id', 'email']
class UserAdmin(ImportExportModelAdmin):
    resource_classes = [UserResource]
    search_fields = ['user_id', 'email']
    list_display = ('user_id', 'email')
#admin.site.register(Users)
admin.site.register(Users, UserAdmin)