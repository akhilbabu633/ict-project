from django.db import models
from uuid import uuid4
# Create your models here.
from ckeditor.fields import RichTextField
import os
from datetime import datetime
from random import randrange

class Users(models.Model):
    user_id= models.CharField(max_length=20, blank=False)
    email= models.CharField(max_length=250, blank=False)