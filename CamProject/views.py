from django.shortcuts import render,redirect,reverse
from django.views.generic import CreateView,UpdateView,ListView,DeleteView, RedirectView,DetailView
# Create your views here.
from django.contrib.auth.models import User
from django.http import Http404
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from braces.views import SetHeadlineMixin,FormValidMessageMixin
from django.urls import reverse_lazy #, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages

from administration.mixins import VerifiedUserMixin
#from administration.mixins import VerifiedUserMixin
from .models import Project
from .forms import *


class ProjectUpdateView(LoginRequiredMixin,SetHeadlineMixin,FormValidMessageMixin,UpdateView):


    def get_queryset(self):
        """Returns Polls that belong to the current user"""
        return Project.objects.filter(created_by=self.request.user)

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()   # This should help to get current user 

        # Next, try looking up by primary key of Usario model.
        queryset = queryset.filter(created_by=self.request.user)


        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            messages.add_message(self.request, messages.INFO, "Cannot update without creating.")
            raise Http404("No user matching this query")
        return obj

class RedirectView(LoginRequiredMixin,RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        try:
            data = Project.objects.get(created_by=self.request.user)
            return reverse('abstract')
        except ObjectDoesNotExist:
            return reverse('project_create')

class ProjectCreateView(LoginRequiredMixin,SetHeadlineMixin,FormValidMessageMixin,CreateView):
    title = "Project Details"
    headline="Enter Your "+ title 
    form_valid_message = title+" added Successfully"
    form_invalid_message = title+" adding failed"
    #queryset = Project.objects.filter(created_by=)
    form_class = ProjectCreateForm
    success_url = reverse_lazy('abstract')
    template_name = "generic/dashboard.html"
    

    def form_valid(self, form):
        user=self.request.user
        #batch=get_batch(user)
        #form.instance.batch=batch
        form.instance.created_by = user
        return super().form_valid(form)
  
    #  def get_success_url(self):
    #      return reverse_lazy('project_detail', kwargs={'pk': self.object.pk})

    def get_success_url(self):
          return reverse_lazy('abstract')



