from allauth.account.models import EmailAddress
from .models import Users
from django.core.exceptions import PermissionDenied
class VerifiedUserMixin:

    def dispatch(self, request, *args, **kwargs):
        user= self.request.user
        try:
            email=EmailAddress.objects.filter(user=user).values()[0]
            print(email)
            if email["verified"]:
                user_details=Users.objects.filter(email=email["email"])[0]
                print(user_details)
                return super().dispatch(request, *args, **kwargs)

            else:
                raise PermissionDenied
        except:
            raise PermissionDenied