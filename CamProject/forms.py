from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from .models import *
class ProjectCreateForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('project_title','project_domain',
        'project_member_name_1', 'project_member_email_1',
        'project_member_name_2', 'project_member_email_2',
         'project_member_name_3', 'project_member_email_3',
         'project_member_name_4', 'project_member_email_4',          
          'guide_name','guide_email',  'project_company_name'                  )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #self.request = kwargs.pop("request")
        #if self.model.objects.get(created_by=self.request.user):
        #    return redirect("miniproject:redirect")

        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Save Project')) 

