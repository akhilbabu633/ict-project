from django.db import models
from uuid import uuid4
# Create your models here.
from ckeditor.fields import RichTextField
import os
from datetime import datetime
from random import randrange
def get_file_path(instance, filename):
    #basename,ext = filename.split('.')
    now = datetime.now()
    dt_string = now.strftime("%d%m%y%H%M%S%f")
    rand=str(randrange(1000,9999))
    filename = "%s_%s_%s_%s" % (instance.uuid,dt_string,rand,filename)
    return os.path.join('CamProject', filename)
def user_directory_path(instance, filename):
  
    # file will be uploaded to MEDIA_ROOT / user_<id>/<filename>
    return 'user_{0}/{1}'.format(instance.user.id, filename)
class Project(models.Model):
    uuid = models.UUIDField(unique=True, default=uuid4, editable=False)
    created_by= models.CharField(max_length=250, blank=False)
    project_title = models.CharField(max_length=250, blank=False)
    project_domain = models.CharField(max_length=250, blank=False)
    project_company_name = models.CharField(max_length=250,blank=True,help_text="Fill this if you have done the project for any company/organization others leave it blank") 
    project_member_name_1 = models.CharField(max_length=250, blank=False)
    project_member_email_1 =models.CharField(max_length=250, blank=False)
    project_member_name_2 = models.CharField(max_length=250,blank=True)
    project_member_email_2 =models.CharField(max_length=250,blank=True)
    project_member_name_3 = models.CharField(max_length=250,blank=True)
    project_member_email_3 =models.CharField(max_length=250,blank=True)
    project_member_name_4 = models.CharField(max_length=250,blank=True)
    project_member_email_4 =models.CharField(max_length=250,blank=True)
    guide_name= models.CharField(max_length=250, blank=False)
    guide_email= models.CharField(max_length=250, blank=False)
    abstract= RichTextField()
    introduction= RichTextField()
    problem_statement= RichTextField()
    objectives= RichTextField()
    system_architecture= RichTextField()
    architecture_image1 = models.ImageField(upload_to = get_file_path,blank=True)
    architecture_image1_caption= models.CharField(max_length=250,blank=True)
    architecture_image2 = models.ImageField(upload_to = get_file_path,blank=True)
    architecture_image2_caption= models.CharField(max_length=250,blank=True)
    architecture_image3 = models.ImageField(upload_to = get_file_path,blank=True)
    architecture_image3_caption= models.CharField(max_length=250,blank=True)
    architecture_image4 = models.ImageField(upload_to = get_file_path,blank=True)
    architecture_image4_caption= models.CharField(max_length=250,blank=True)
    architecture_image5 = models.ImageField(upload_to = get_file_path,blank=True)
    architecture_image5_caption= models.CharField(max_length=250,blank=True)
    implementation= RichTextField()
    social_relevance= RichTextField()
    applications=  RichTextField()
    conclusion=  RichTextField()
    references=  RichTextField()

