from django.apps import AppConfig


class CamprojectConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'CamProject'
